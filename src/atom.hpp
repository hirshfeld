// All units is under a.u.
#ifndef HIRSHFELD_ATOM_H
#define HIRSHFELD_ATOM_H
#include <iosfwd>

namespace hirshfeld {
  class Atom {
  public:
    Atom(int atomicnumber_, double x_, double y_, double z_)
    : m_atomicnumber(atomicnumber_), m_x(x_), m_y(y_), m_z(z_)
    {}

    int atomicnumber() const {return m_atomicnumber;}
    double x() const {return m_x;}
    double y() const {return m_y;}
    double z() const {return m_z;}
  private:
    int m_atomicnumber;  //Atomic number
    double m_x;
    double m_y;
    double m_z;
  };
  std::ostream& operator<<(std::ostream& os, const Atom& atom);
};

#endif
