#ifndef HIRSHFELD_MOLECULE_H
#define HIRSHFELD_MOLECULE_H

#include <vector>
#include <iosfwd>

#include <boost/noncopyable.hpp>

namespace hirshfeld {
  class Atom;
  class Contraction; 
  class Molecule : private boost::noncopyable
  {
  public:
    Molecule();
    explicit Molecule(std::istream& is);
    ~Molecule();

    operator void*() const {return m_dirty?0:(void *)(1);}

    void read(std::istream& is);
    
    double density(double x, double y, double z) const;
    int atmnum() const;
    const Atom& atom(int idx) const;

    friend std::ostream&
    operator<<(std::ostream& os, const Molecule& mol);
  private:
    bool m_dirty;
    bool closeshell;
    std::vector<Atom> m_atoms;
    std::vector<Contraction *> conts;
    std::vector<std::vector<double> > density_matrix;

    void check();
    void init();
    void initatoms();
    void initconts();
    void initdensitymatrix();
    void init_S(int shell_idx);
    void init_P(int shell_idx);
    void init_SP(int shell_idx);
    void init_D(int shell_idx);
    void init_D5(int shell_idx);
  };
}
#endif
