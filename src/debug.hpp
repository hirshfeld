#ifndef HIRSHFELD_DEBUG_HPP
#define HIRSHFELD_DEBUG_HPP

#include <ostream>

namespace hirshfeld {

#ifdef DEBUG
extern std::ostream& debug;
#else

class DummyOstream {
};

template<class C>
inline DummyOstream& operator<<(DummyOstream& os,
                         const C&)
{
  return os;
}
extern DummyOstream& debug;
#endif

}

#endif
