#include "config.h"
#include "atomdata.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cassert>
#include <algorithm>
#include "slater.hpp"
#include "base_directory.hpp"
#include "debug.hpp"

using namespace std;
using namespace hirshfeld;

Atomdata::Atomdata()
  : m_dirty(false)
{
}

// FIXME: don't assume the data have 100 lines
// FIXME: don't assume data is sorted
// FIXME: need check all data is valid (i.e. all data should be positive)
Atomdata::Atomdata(int atomicnumber)
  : m_atomicnumber(atomicnumber),
    m_dirty(false)
{
  ostringstream resource;
  resource << "hirshfeld/" << atomicnumber << ".data";

  string fname = load_first_data(resource.str());
  if(fname.empty()) {
    debug << string("open ") + resource.str() + " failed.";
    m_dirty = true;
    return;
  }
  
  ifstream in(fname.c_str());
  if(!in) {
    cerr << "Can't open file " << fname << '\n';
    exit(1);
  }
  for(int i = 0; i < 100; i++) {
    double tmp1;
    double tmp2;
    in >> tmp1 >> tmp2;
    assert(in);
    m_r.push_back(tmp1);
    assert(m_r.back() >= 0.0);
    assert(i == 0 or m_r[i] >= m_r[i-1]);
    m_density.push_back(tmp2);
    assert(m_density.back() >= 0.0);
    debug << tmp1 << '\t' << tmp2 << '\n';
  }
  m_rm = Slater_radius(atomicnumber);
}

double Atomdata::rm() const {
	return m_rm;
}

double Atomdata::density(double r) const{
  assert(*this);
  if(r <= m_r[0]) {
    return m_density[0];
  }
  
  if(r >= m_r.back()) {
    return m_density.back();
  }

  int idx = lower_bound(m_r.begin(), m_r.end(), r) - m_r.begin();
  assert(idx != 0);
  double x1 = m_r[idx-1];
  double x2 = m_r[idx];
  double y1 = m_density[idx-1];
  double y2 = m_density[idx];
  return y1 + (y2 - y1) * (r - x1) / (x2 - x1);
  // debug << "Atomdata::density: " << number() << '\t' << r << '\t' << idx << '\t' << result << '\n';
}

int Atomdata::number() const {
	return m_atomicnumber;
}
