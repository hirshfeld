#ifndef HIRSHFELD_BASE_DIRCTORY_HPP
#define HIRSHFELD_BASE_DIRCTORY_HPP

#include <string>

namespace hirshfeld {

std::string
load_first_data(const std::string& resource);

}

#endif


