#ifndef HIRSHFELD_SLATER_HPP
#define HIRSHFLED_SLATER_HPP

namespace hirshfeld {

//J.C. Slater J. Chem. Phys. 41. 3199(1964)
double Slater_radius(int atomicnumber);

}

#endif
