#ifndef HIRSHFELD_LEBEDEV_HPP
#define HIRSHFELD_LEBEDEV_HPP

namespace hirshfeld {

int Lebedev_Laikov_sphere (int N, double *X, double *Y, double *Z, double *W);

}

#endif
