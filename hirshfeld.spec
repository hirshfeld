Name:           hirshfeld
Version:        0.2.2
Release:        1%{?dist}
Summary:        Calculation Hirshfeld charge from gaussian's fchk file

Group:          Applications/Science
License:        GPL
URL:            http://code.google.com/p/hirshfeld/
Source0:        http://hirshfeld.googlecode.com/files/hirshfeld-0.2.2.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:  
#Requires:       

%description
Hirshfeld can help you calculate the Hirshfeld charge from the
gaussian's fchk file. Hirshfeld charge is defined in
doi:10.1007/BF00549096 .

Following is an example for calculation hirshfeld charge.

$ hirshfeld HCN.fchk 
No.     Atomic  elctron         charge
1       1         0.871666      +0.128334
2       6         5.947401      +0.052599
3       7         7.180643      -0.180643

I write this program because gaussian03's hirshfeld charge module is
buggy and not free.

%prep
%setup -q


%build
%configure 
#--disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_bindir}/hirshfeld
%{_libdir}/hirshfeld
%{_datadir}/hirshfeld


%changelog
* Sat Apr 14 2007 bbbush <bbbush.yuan@gmail.com> - 0.2.2-1
- Initial import 
- No dependence info.
