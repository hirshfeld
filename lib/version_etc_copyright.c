#include <config.h>

/* Specification.  */
#include "version-etc.h"

const char version_etc_copyright[] =
  /* Do *not* mark this string for translation.  %s is a copyright
     symbol suitable for this locale, and %d is the copyright
     year.  */
  "Copyright %s %d LI Daobing <lidaobing@gmail.com>";
